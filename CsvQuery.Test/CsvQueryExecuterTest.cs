﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CsvQuery.Library;

namespace CsvQuery.Test
{
    [TestClass]
    public class CsvQueryExecuterTest
    {
        string[] ReturnInitialTestData()
        {
            return new string[]
            {
                "id,name,hometown",
                "1,Kiara,Lune",
                "2,Mona,Henley-on-Thames",
                "3,Kiayada,Villers-aux-Tours",
                "4,Karly,Hillsboro",
                "5,Igor,Oranienburg"
            };
        }

        [TestMethod]
        public void SelectIdTest()
        {
            //Arrange
            var executer = new CsvQueryExecuter(ReturnInitialTestData());
            var query = "SELECT id";
            var resultExpected = new string[6, 1]
            {
                {"id"},{"1"},{"2"},{"3"},{"4"},{"5"}
            };

            //Act
            var result = executer.ExecuteQuery(query);

            //Assert
            CollectionAssert.AreEqual(resultExpected, result);
        }

        [TestMethod]
        public void SelectIdAndNameTest()
        {
            //Arrange
            var executer = new CsvQueryExecuter(ReturnInitialTestData());
            var query = "SELECT id, name";
            var resultExpected = new string[6, 2]
            {
                {"id","name"},
                {"1","Kiara"},
                {"2","Mona"},
                {"3","Kiayada"},
                {"4","Karly"},
                {"5","Igor"}
            };

            //Act
            var result = executer.ExecuteQuery(query);

            //Assert
            CollectionAssert.AreEqual(resultExpected, result);
        }

        [TestMethod]
        public void SelectIdAndNameWithLimitTest()
        {
            //Arrange
            var executer = new CsvQueryExecuter(ReturnInitialTestData());
            var query = " SELECT id, name LIMIT 1";
            var resultExpected = new string[2, 2]
            {
                {"id","name"},
                {"1","Kiara"}
            };

            //Act
            var result = executer.ExecuteQuery(query);

            //Assert
            CollectionAssert.AreEqual(resultExpected, result);
        }

        [TestMethod]
        public void SumIdTest()
        {
            //Arrange
            var executer = new CsvQueryExecuter(ReturnInitialTestData());
            var query = "SUM id";
            var resultExpected = new string[1, 1]
            {
                {"15"}
            };

            //Act
            var result = executer.ExecuteQuery(query);

            //Assert
            CollectionAssert.AreEqual(resultExpected, result);
        }

        [TestMethod]
        public void ShowTest()
        {
            //Arrange
            var executer = new CsvQueryExecuter(ReturnInitialTestData());
            var query = "SHOW";
            var resultExpected = new string[1, 1]
            {
                {"id, name, hometown"}
            };

            //Act
            var result = executer.ExecuteQuery(query);

            //Assert
            CollectionAssert.AreEqual(resultExpected, result);
        }

        [TestMethod]
        public void FindDashTest()
        {
            //Arrange
            var executer = new CsvQueryExecuter(ReturnInitialTestData());
            var query = " FIND \"-\"";
            var resultExpected = new string[3, 3]
            {
                {"id","name","hometown"},
                {"2","Mona","Henley-on-Thames"},
                {"3","Kiayada","Villers-aux-Tours"},
            };

            //Act
            var result = executer.ExecuteQuery(query);

            //Assert
            CollectionAssert.AreEqual(resultExpected, result);
        }
    }
}
