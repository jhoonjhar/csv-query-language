﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;

namespace CsvQuery.Library
{
    public class CsvQueryExecuter
    {
        const string DefaulfPath = @"C:\temp\default.csv";
        public string[,] Csv { get; private set; }

        public CsvQueryExecuter()
            : this(DefaulfPath)
        { }

        public CsvQueryExecuter(string filePath)
            : this(GetLinesFromFile(filePath))
        { }

        private static string[] GetLinesFromFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                return new string[0];
            }
            return File.ReadAllLines(filePath);
        }

        public CsvQueryExecuter(IEnumerable<string> csvLines)
        {
            MakeNewCsv(csvLines);
        }

        public void MakeNewCsv()
        {
            MakeNewCsv(DefaulfPath);
        }

        public void MakeNewCsv(string path)
        {
            MakeNewCsv(GetLinesFromFile(path));
        }

        public void MakeNewCsv(IEnumerable<string> csvLines)
        {
            if (csvLines.ToList().Count <= 1)
	        {
		        this.Csv = new string[0,0];
                return;
	        }
            List<List<string>> csvAsNestedLists = new List<List<string>>();
            foreach (var csvLine in csvLines)
            {
                List<string> lineAsList = csvLine.Split(new char[] { ',' }).ToList();
                csvAsNestedLists.Add(lineAsList);
            }
            this.Csv = new string[csvAsNestedLists.Count, csvAsNestedLists[0].Count];
            for (int i = 0; i < csvAsNestedLists.Count; i++)
            {
                for (int j = 0; j < csvAsNestedLists[i].Count; j++)
                {
                    this.Csv[i, j] = csvAsNestedLists[i][j];
                }
            }
        }

        //very sensitive, throws a lot of exceptions
        public string[,] ExecuteQuery(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
            {
                throw new ArgumentNullException("query " + query +" is not valid");
            }
            
            query = query.ToLower();
            var queryParts = query.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            switch (queryParts[0])
            {
                case "select" :
                    return SelectHandler(queryParts);
                case "sum":
                    return SumHandler(queryParts);
                case "show":
                    return ShowHandler(queryParts);
                case "find":
                    return FindHandler(queryParts);
                default:
                    throw new ArgumentException("the first command \"" + queryParts[0] + "\" is unknown!");
            }
        }

        private string[,] SelectHandler(string[] queryParts)
        {
            if (queryParts[0] != "select")
            {
                throw new ArgumentException("the expected first element is \"SELECT\"");
            }
            if (queryParts.Length < 2)
            {
                throw new ArgumentException("expected at least two words in the query for this comand");
            }

            int queryPartIndex = 1;
            int limit = int.MaxValue - 1;
            List<string> columns = new List<string>();

            //checking for ",", so to read all arguments before LIMIT comand or the end of query
            while (queryParts[queryPartIndex][queryParts[queryPartIndex].Length - 1] == ',')
            {
                string column = queryParts[queryPartIndex].Substring(0, queryParts[queryPartIndex].Length - 1);
                if (!IsValidColumn(column, this.Csv))
                {
                    string[,] validColumns = ShowHandler(new string[1] {"show"});
                    throw new ArgumentException("There is no such column as \"" +
                        column +
                        "\"!. Columns are: " +
                        string.Join(", ", validColumns[0, 0]));
                }
                columns.Add(column);
                queryPartIndex++;
            }

            string lastColumn = queryParts[queryPartIndex];
            if (!IsValidColumn(lastColumn, this.Csv))
            {
                string[,] validColumns = ShowHandler(new string[1] { "show" });
                throw new ArgumentException("There is no such column as \"" +
                    lastColumn +
                    "\"!. Columns are: " +
                    string.Join(", ", validColumns[0, 0]));
            }
            columns.Add(lastColumn);
            queryPartIndex++;

            if (queryParts.Length != queryPartIndex &&
                queryParts[queryPartIndex] != "limit")
            {
                throw new ArgumentException("needs a comma (,) on revious column, \"LIMIT\" comand, or end of the query!");
            }

            if (queryParts.Length != queryPartIndex)
            {
                if (queryParts.Length != queryPartIndex + 2)
                {
                    throw new ArgumentException("needs one parameter after the \"LIMIT\" command");
                }
                bool castLimit = int.TryParse(queryParts[queryPartIndex + 1], out limit);
                if (!castLimit)
                {
                    throw new ArgumentException("after \"LIMIT\" command it should be a valid number to set the count of the result");
                }
            }

            int tableLen = Math.Min(limit + 1, this.Csv.GetLength(0));

            string[,] result = new string[tableLen, columns.Count];
            //header
            for (int i = 0; i < columns.Count; i++)
            {
                result[0, i] = columns[i];
            }

            //body
            for (int i = 1; i < tableLen; i++)
            {
                for (int j = 0; j < columns.Count; j++)
                {
                    int columnIndex = FindColumnIndex(columns[j]);
                    result[i, j] = this.Csv[i, columnIndex];
                }
            }
            if (result.GetLength(0) < 2)
            {
                throw new ArgumentException("No results matches your search");
            }

            return result;
        }

        private int FindColumnIndex(string columnName)
        {
            for (int i = 0; i < this.Csv.GetLength(1); i++)
            {
                if (this.Csv[0, i] == columnName)
                {
                    return i;
                }
            }
            throw new ArgumentException("The column " + columnName + "was not found");
        }

        private bool IsValidColumn(string column, string[,] csv)
        {
            for (int i = 0; i < csv.GetLength(1); i++)
            {
                if (csv[0,i] == column)
                {
                    return true;
                }
            }
            return false;
        }

        private string[,] SumHandler(string[] queryParts)
        {
            if (queryParts.Length == 0)
            {
                throw new ArgumentException("Command \"SUM\" is expected");
            }
            if (queryParts[0] != "sum")
            {
                throw new ArgumentException("Command \"SUM\" is expected");
            }
            if (queryParts.Length != 2)
            {
                throw new ArgumentException("Needs only one param after \"SUM\" comand");
            }

            int ColumnIndex = FindColumnIndex(queryParts[1]);
            long sum = 0;
            for (int i = 1; i < this.Csv.GetLength(0); i++)
            {
                int columnAsInt = 0;
                bool IsCorectInt = int.TryParse(this.Csv[i, ColumnIndex], out columnAsInt);
                if (!IsCorectInt)
                {
                    throw new ArgumentException("The column number can't be cast: " + this.Csv[i, ColumnIndex]);
                }
                try 
	            {	        
		            sum += columnAsInt;
	            }
	            catch (Exception)
	            {
                    throw new OverflowException("The number is too big");
	            }
            }
            string[,] result = new string[1,1];
            result[0,0] = sum.ToString();
            return result;
        }

        private string[,] ShowHandler(string[] queryParts)
        {
            if (queryParts.Length > 1 ||
                queryParts[0] != "show")
            {
                throw new ArgumentException("only \"SHOW\" comand is expected here");
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < this.Csv.GetLength(1); i++)
			{
			    sb.Append(this.Csv[0,i]);
                sb.Append(", ");
			}
            if (sb.Length > 2)
	        {
		        sb.Length -= 2;
	        }
            string[,] result = new string[1,1];
            result[0, 0]= sb.ToString();
            return result;
        }

        private string[,] FindHandler(string[] queryParts)
        {
            if (queryParts.Length == 0)
            {
                throw new ArgumentException("Command \"FIND\" is expected");
            }
            if (queryParts[0] != "find")
            {
                throw new ArgumentException("Command \"FIND\" is expected");
            }
            if (queryParts.Length != 2)
            {
                throw new ArgumentException("Needs only one param after \"FIND\" comand");
            }

            string SearchParam = queryParts[1];

            List<string[]> resultAsList = new List<string[]>();
            string[] header = new string[this.Csv.GetLength(1)];
            for (int i = 0; i < this.Csv.GetLength(1); i++)
            {
                header[i] = this.Csv[0, i];
            }
            resultAsList.Add(header);
            for (int i = 0; i < this.Csv.GetLength(0); i++)
            {
                for (int j = 0; j < this.Csv.GetLength(1); j++)
                {
                    if (SearchParam.IndexOf('"') == 0 &&
                        SearchParam.LastIndexOf('"') == SearchParam.Length - 1)
                    {
                        if (SearchParam.Length < 3)
	                    {
		                    throw new ArgumentException("Search param needs to have some string in between the double quotes");
	                    }
                        string SearchParamWithNoQuotes = SearchParam.Substring(1, SearchParam.Length - 2);
                        if (this.Csv[i, j].ToLower().Contains(SearchParamWithNoQuotes))
                        {
                            string[] row = new string[this.Csv.GetLength(1)];
                            for (int g = 0; g < this.Csv.GetLength(1); g++)
                            {
                                row[g] = this.Csv[i, g];
                            }
                            resultAsList.Add(row);
                            break;
                        }
                    }
                    else
                    {
                        int ArgumentAsNum = 0;
                        bool IsNumber = int.TryParse(SearchParam, out ArgumentAsNum);
                        if (!IsNumber)
                        {
                            throw new ArithmeticException("The argument of FIND is not a number (or too big), if it is a string, it should be in double quotes (\")");
                        }
                        if (this.Csv[i, j].ToLower() == SearchParam)
                        {
                            string[] row = new string[this.Csv.GetLength(1)];
                            for (int g = 0; g < this.Csv.GetLength(1); g++)
                            {
                                row[g] = this.Csv[i, g];
                            }
                            resultAsList.Add(row);
                            break;
                        }
                    }
                }
            }

            string[,] result = MakeTableFromList(resultAsList);
            return result;
        }

        private string[,] MakeTableFromList(List<string[]> resultAsList)
        {
            if (resultAsList.Count <= 1)
            {
                throw new ArgumentException("No result are found");
            }
            string[,] result = new string[resultAsList.Count, resultAsList[0].Length];
            for (int i = 0; i < resultAsList.Count; i++)
            {
                for (int j = 0; j < resultAsList[i].Length; j++)
                {
                    result[i, j] = resultAsList[i][j];
                }
            }
            return result;
        }
    }
}
