﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvQuery.Library;

namespace CsvQuery.UI.ConsoleUI
{
    class CsvQueryConsole
    {
        static void Main()
        {
            CsvQueryExecuter csv = new CsvQueryExecuter();

            while (true)
            {
                Console.Clear();
                Console.WriteLine("type \"QUIT\" to exit the program");
                Console.Write("query>");
                string query = Console.ReadLine();
                if (query.ToLower() == "quit")
                {
                    break;
                }
                try
                {
                    Console.WriteLine(ConsoleCsvData(csv.ExecuteQuery(query)));
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine("ArgumentException: " + ex.Message);
                }
                catch (OverflowException ex)
                {
                    Console.WriteLine("OverflowException: " + ex.Message);
                }
                Console.ReadLine();
            }
        }

        protected static string ConsoleCsvData(string[,] data)
        {
            if (data.GetLength(0) <= 1)
            {
                //If there is on one row - it's header only, there is no data
                return data[0, 0];
            }
            StringBuilder sb = new StringBuilder();

            int[] rowsWidth = GetRowWidth(data);
            for (int i = 0; i < data.GetLength(0); i++)
            {
                if (i == 1) //after header
                {
                    sb.AppendLine(MakeSeparatorLine(rowsWidth));
                }
                sb.Append('|');
                for (int j = 0; j < data.GetLength(1); j++)
                {
                    sb.Append(data[i,j].PadRight(rowsWidth[j]));
                    sb.Append('|');
                }
                
                sb.AppendLine();
            }

            return sb.ToString();
        }

        private static string MakeSeparatorLine(int[] rowsWidth)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append('|');
            for (int i = 0; i < rowsWidth.Length; i++)
            {
                sb.Append(new string('-', rowsWidth[i]));
                sb.Append('|');
            }
            return sb.ToString();
        }

        private static int[] GetRowWidth(string[,] data)
        {
            int[] rowsWidth = new int[data.GetLength(1)];

            for (int i = 0; i < data.GetLength(1); i++)
            {
                int maxWidthForCol = 0;

                for (int j = 0; j < data.GetLength(0); j++)
                {
                    if (data[j,i].Length > maxWidthForCol)
                    {
                        maxWidthForCol = data[j, i].Length;
                    }
                }

                rowsWidth[i] = maxWidthForCol;
            }

            return rowsWidth;
        }
    }
}
